package sortersTesterClasses;

import sorterClasses.BubbleSortSorter;

public class tester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		 Entero[] thing = new Entero[4];
		
		 
		 thing[0] = new Entero(8);
		 thing[1] = new Entero(4);
		 thing[2] = new Entero(3);
		 thing[3] = new Entero(7);
		 
		 for (int i = 0; i < thing.length; i++) {
			 System.out.println("before:"+thing[i].getValue());
			}
		 
		 tester(thing);
		
		 for (int i = 0; i < thing.length; i++) {
			 System.out.println("after:"+thing[i].getValue());
			}
	}
	
	public static void tester(Entero[] e)
	{
		BubbleSortSorter<Entero> bubbles = new BubbleSortSorter<Entero>();
		bubbles.sort(e, null);
	}

}
