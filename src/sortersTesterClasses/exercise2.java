package sortersTesterClasses;

import sorterClasses.InsertionSortSorter;

public class exercise2 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		 Integer[] ex2array = {5, 9, 20, 22, 20, 5, 4, 13, 17, 8, 22, 1, 3, 7, 11, 9, 10} ;
		 
		 for (int i = 0; i < ex2array.length; i++) {
			 System.out.println("before:"+ex2array[i]);
			}
		 
		 InsertionSortSorter<Integer> sorterEx2 = new InsertionSortSorter<Integer>(); 
	sorterEx2.sort(ex2array, new IntegerComparator1());
	for (int i = 0; i < ex2array.length; i++) {
		 System.out.println("after comp1:"+ex2array[i]);
		}
	sorterEx2.sort(ex2array, new IntegerComparator2());
	for (int i = 0; i < ex2array.length; i++) {
		 System.out.println("after comp2:"+ex2array[i]);
		}
	}
	
	

}
